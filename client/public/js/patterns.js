// these are from https://en.wikipedia.org/wiki/Conway's_Game_of_Life#Examples_of_patterns
// and http://conwaylife.com/wiki/Copperhead

const GameOfLifePatterns = {
  Patterns: {
    Block: [
      '....',
      '.OO.',
      '.OO.',
      '....',
    ],

    Beehive: [
      '......',
      '..OO..',
      '.O..O.',
      '..OO..',
      '......',
    ],

    Loaf: [
      '......',
      '..OO..',
      '.O..O.',
      '..O.O.',
      '...O..',
      '......',
    ],

    Boat: [
      '.....',
      '.OO..',
      '.O.O.',
      '..O..',
      '.....',
    ],

    Tub: [
      '.....',
      '..O..',
      '.O.O.',
      '..O..',
      '.....',
    ],

    Blinker: [
      '.....',
      '.....',
      '.OOO.',
      '.....',
      '.....',
    ],

    Beacon: [
      '......',
      '.OO...',
      '.O....',
      '....O.',
      '...OO.',
      '......',
    ],

    Pulsar: [
      '.................',
      '.................',
      '....OOO...OOO....',
      '.................',
      '..O....O.O....O..',
      '..O....O.O....O..',
      '..O....O.O....O..',
      '....OOO...OOO....',
      '.................',
      '....OOO...OOO....',
      '..O....O.O....O..',
      '..O....O.O....O..',
      '..O....O.O....O..',
      '.................',
      '....OOO...OOO....',
      '.................',
      '.................',
    ],

    Glider: [
      '.....',
      '..O..',
      '...O.',
      '.OOO.',
      '.....',
    ],

    Copperhead: [
      '............',
      '.....OO.....',
      '....OOOO....',
      '............',
      '...OOOOOO...',
      '....OOOO....',
      '............',
      '...OO..OO...',
      '.OO.O..O.OO.',
      '....O..O....',
      '............',
      '............',
      '.....OO.....',
      '.....OO.....',
      '............',
    ],
  },

  patternToCoords: (pattern) => {
    const [w, h] = [pattern[0].length, pattern.length];
    const coords = [];
    for (let y = 0; y < h; y += 1) {
      for (let x = 0; x < w; x += 1) {
        if (pattern[y][x] === 'O') coords.push([x, y]);
      }
    }
    return { w, h, coords };
  },
};
