/* globals document, window, Intl, console */

// these are loaded via <script> tags
// TODO: use ES6 modules instead
/* globals io, GameOfLifePatterns */

// default size of each cell in pixels
// the actual grid sizes will be given by the server
const CELL_SIZE = 4;
const GRID_WIDTH = 256;
const GRID_HEIGHT = 192;


document.addEventListener('DOMContentLoaded', () => {
  'use strict';

  // colours coming from the server are already in hex, but not zero padded
  // make them zero padded and add a '#' in front
  function toHTMLColour(colour) {
    return `#${(`000000${colour}`).slice(-6)}`;
  }

  function drawDot(ctx, x, y, colour) {
    ctx.fillStyle = colour;
    ctx.fillRect(x, y, 1, 1);
  }

  const localNumberFormat = new Intl.NumberFormat();

  const updateNotifications = (currentNotification, msg = undefined) => {
    ['connect', 'reconnecting', 'connect-error'].forEach((key) => {
      const showNotification = currentNotification === key;
      const elem = document.getElementById('notification-' + key);
      elem.style.visibility = showNotification ? 'visible' : 'collapse';

      if (msg !== undefined) {
        elem.innerText = msg;
      }

      // hack to restart the CSS animation
      if (currentNotification === 'connect' && key === 'connect') {
        elem.classList.remove('fadeout');
        void elem.offsetWidth;
        elem.classList.add('fadeout');
      }
    });
  };

  let cellSize = CELL_SIZE;
  let gridWidth = GRID_WIDTH;
  let gridHeight = GRID_HEIGHT;
  let isMouseDown = false;
  let myColour = '#000000';

  // resize the universe based on window size
  const gameDiv = document.getElementById('game');
  const canvas = document.getElementById('board');
  const updates = document.getElementById('updates');
  const canvasCtx = canvas.getContext('2d');
  const updatesCtx = updates.getContext('2d');
  const panelDiv = document.getElementById('panel');
  const onWindowResize = () => {
    const windowWidth = window.innerWidth - (gameDiv.offsetLeft * 2);
    const windowHeight = window.innerHeight - (gameDiv.offsetTop + 2);
    const canvasWidth = Math.min(windowWidth, Math.floor(windowHeight * gridWidth / gridHeight));
    const canvasHeight = Math.round(canvasWidth * gridHeight / gridWidth);

    canvas.width = canvasWidth;
    canvas.height = canvasHeight;
    updates.width = canvasWidth;
    updates.height = canvasHeight;
    gameDiv.style.width = `${canvasWidth + 2}px`;
    gameDiv.style.height = `${canvasHeight + 2}px`;
    if (windowWidth < 1124) {
      panelDiv.style.top = `${canvasHeight + 4 + 30}px`;
    } else {
      panelDiv.style.top = '';
    }

    cellSize = canvasWidth / gridWidth;
    canvasCtx.scale(cellSize, cellSize);
    canvasCtx.imageSmoothingEnabled = false;  // needs to be set each time scale changes; bug?
  };
  onWindowResize();
  window.addEventListener('resize', onWindowResize);
  document.addEventListener('unload', () => {
    window.removeEventListener('resize', onWindowResize);
  });

  // create a buffer to draw
  const buffer = document.createElement('canvas');
  buffer.width = gridWidth;
  buffer.height = gridHeight;
  const ctx = buffer.getContext('2d');
  ctx.imageSmoothingEnabled = false;

  // connect to server, with the token if we have one
  const socket = io.connect('/life', {
    query: { token: window.localStorage.getItem('life:token') }
  });

  // on init, we are assigned a colour, which we show to the user
  let inited = false;
  socket.on('init', (data) => {
    // bail out if wrong version
    if (data.version !== 1) {
      updateNotifications('connect_error', 'Version mismatch with server, try reloading the page');
      return;
    }

    myColour = toHTMLColour(data.colour);
    document.getElementById('myColour').style.backgroundColor = myColour;
    if (data.token) window.localStorage.setItem('life:token', data.token);

    gridWidth = Number(data.width);
    gridHeight = Number(data.height);
    // update the sizes
    buffer.width = gridWidth;
    buffer.height = gridHeight;
    onWindowResize();

    inited = true;
  });

  // the main update loop
  const genNumElem = document.getElementById('gen');
  const popNumElem = document.getElementById('pop');
  socket.on('t', (update) => {
    if (!inited) return;

    // update the current generation #
    genNumElem.innerText = localNumberFormat.format(update.gen);
    popNumElem.innerText = localNumberFormat.format(update.pop);

    // clear the board
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, update.board.width, update.board.height);

    // draw the whole state again, naively
    update.board.rows.forEach((row, y) => {
      for (const x in row) {
        drawDot(ctx, x, y, toHTMLColour(row[x]));
      }
    });

    // render the whole buffer
    canvasCtx.drawImage(buffer, 0, 0);
    if (!isMouseDown) updatesCtx.clearRect(0, 0, updates.width, updates.height);
  });

  // Implements giving birth to cells by clicking
  // We queue up the coordinates while the mouse button is held down,
  // and sent in one go upon release.
  // Not only this saves bandwidth, playability is better as this allows
  // for multiple cells to be born, otherwise only a single cell is born
  // which quickly dies.
  const queuedCoords = [];
  const queueCoord = (x, y) => {
    queuedCoords.push([x, y]);
    updatesCtx.save();
    updatesCtx.fillStyle = '#ffffff';
    updatesCtx.strokeStyle = myColour;
    updatesCtx.fillRect(x * cellSize, y * cellSize, cellSize, cellSize);
    updatesCtx.strokeRect(x * cellSize, y * cellSize, cellSize, cellSize);
    updatesCtx.restore();
  };
  const submitQueuedCoords = () => {
    socket.emit('birth', queuedCoords);
    queuedCoords.length = 0;
  };

  const queueMouseDown = (ev) => {
    const x = Math.floor((ev.offsetX) / cellSize);
    const y = Math.floor((ev.offsetY) / cellSize);
    queueCoord(x, y);
  };
  updates.addEventListener('mousedown', (ev) => {
    if (ev.button === 0) {
      isMouseDown = true;
      queueMouseDown(ev);
    }
  });
  updates.addEventListener('mousemove', (ev) => {
    if (isMouseDown && ev.button === 0) {
      queueMouseDown(ev);
    }
  });
  updates.addEventListener('mouseup', (ev) => {
    if (isMouseDown && ev.button === 0) {
      isMouseDown = false;
      submitQueuedCoords();
    }
  });

  // populate the patterns menu
  const patternsMenu = document.getElementById('pattern-menu');
  Object.keys(GameOfLifePatterns.Patterns).forEach((key) => {
    const pattern = GameOfLifePatterns.Patterns[key];
    const { w, h, coords } = GameOfLifePatterns.patternToCoords(pattern);
    const menuItemImage = document.createElement('canvas');
    menuItemImage.width = w * 4;
    menuItemImage.height = h * 4;
    const menuItemImageCtx = menuItemImage.getContext('2d');
    menuItemImageCtx.scale(4, 4);
    coords.forEach(([x, y]) => {
      drawDot(menuItemImageCtx, x, y, myColour);
    });

    const menuItemText = document.createElement('span');
    menuItemText.innerText = key;

    const menuItem = document.createElement('a');
    menuItem.className = 'dropdown-item';
    menuItem.href = '#';
    menuItem.appendChild(menuItemImage);
    menuItem.appendChild(menuItemText);
    menuItem.setAttribute('title', 'Click to add this pattern at a random location');
    patternsMenu.appendChild(menuItem);

    menuItem.onclick = () => {
      // find a random (x, y) within the grid
      const left = Math.floor(Math.random() * (gridWidth - 8 - w) + 4);
      const top = Math.floor(Math.random() * (gridHeight - 8 - h) + 4);
      coords.forEach(([x, y]) => queueCoord(x + left, y + top));
      submitQueuedCoords();
    };
  });

  // show connection status
  socket.on('connect', () => {
    console.log('connected');
    updateNotifications('connect');
  });
  socket.on('connect_error', () => {
    console.log('connect error');
    updateNotifications('connect-error', 'Connection failed. Try reloading the page.');
  });
  socket.on('reconnecting', () => {
    console.log('reconnecting');
    updateNotifications('reconnecting');
  });
});
