# Multiplayer Game of Life in Colour

This is an implementation of [Conway’s Game of Life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life) as a Web application.
The game actually runs on the server side, and all clients connecting to the same server share the same game universe.

See https://multiplayer-life.herokuapp.com/ for a live demo.

## How to Run

This has been developed with Node.js 8.9. The client has been tested with Chrome 64 and Firefox 59.
Older versions may work but untested.

To build:
```
npm install
npm test
npm run start
```

There are some environmental variables for configuration:

| Variable | Description |
|----------|-------------|
| `PORT` | The HTTP port to listen on, default 3000 |
| `TICK_INTERVAL` | The number of milliseconds between each game tick, default 500 |
| `BOARD_WIDTH` | The width of the game universe, default 256 |
| `BOARD_HEIGHT` | The height of the game universe, default 192 |
| `LOG_LEVEL` | Level of logging, default `info` (see [here](https://github.com/winstonjs/winston/tree/2.x#logging-levels)) |
| `JWT_SECRET` | Secret string used to sign the session token |

Once running, just point your browser at eg. http://localhost:3000


## Multiplayer

As soon as the Web app is loaded and connected to the server, you are assigned with a random colour. You can click anywhere on
the game to make cells alive.

When a new 'offspring' cell is born when there are 3 alive neighbouring cells, the newborn's colour is the average of the 3 neighbours.

## Technical Details

* The server is the single source of truth for the game state. Any modification the client wants to make (eg. making cells alive)
  are sent to the server to be queued, and clients do not directly modify the state. Queued requests are processed on the server
  at every game tick. This is to make sure all clients see the same game state.

* The game universe is implemented as a straightforward 2D array of cells, each cell holding a 24-bit colour value.
  Dead cells are represented as the white colour `0xffffff`. See `board.js`. This makes the Life logic very simple to implement, and it
  is very easy for newly connected clients to retrieve the entire game state.

* However, because the entire game state is held in the server process's memory, this is not yet horizontally scalable by adding
  servers. If it is desirable to share the same game state across more than tens of clients, the game state should probablt
  be stored somewhere else, using eg. Redis.

* The edges wraps around: the left end right edges are stitched together, and so are the top and bottom, creating a somewhat 'infinite'
  universe that Life games have. For a true infinite universe, a different data structure such as a quadtree should be used.

* The client/server communication is handled using socket.io.
  Currently the entire state of the universe is sent to the clients on every tick. It works reasonably well with a few clients; but
  clearly this is not scalable.

* The client does not use any UI frameworks such as React. While this makes the code simple to build (there is no build process),
  using React or Vue.js would probably make the UI code a bit simpler.

* To improve UX, the UI allows for 'drawing' multiple cells as the left mouse button is being held down. The drawn cells are queued and
  then sent to the server all at once, when the button is released; this is to make it easier to make multiple cells alive in one tick,
  as otherwise the lone alive cells would die very quickly. To differentiate between the queued cells and actual alive cells, the
  queued cells are not filled in.

## Coding Style

We try to adhere to the [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript) as closely as possible without using Babel.

## Known Bugs

* UI: with some browser window's aspect ratios, sometimes the description panel on the right overlaps the game universe.

* UI: sometimes the drawn cells disappears for 1 tick before it appears in the universe.

* The game state is not persisted and will not survive a server crash.

## TODO

* Only send the game state delta to clients on eack tick

* More tests...

## Contact

Roger So <roger.so@gmail.com>
