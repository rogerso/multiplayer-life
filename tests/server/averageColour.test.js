const averageColour = require('../../server/averageColour');

/* globals test, expect */
test('average of same colours', () => {
  const testCases = [0x00ff0000, 0x0000ff00, 0x000000ff];
  testCases.forEach((testCase) => {
    expect(averageColour([testCase, testCase, testCase])).toBe(testCase);
  });
});

test('average of 3 different primaries', () => {
  const testCases = [[0x00ff0000, 0x0000ff00, 0x000000ff, 0x00555555]];
  testCases.forEach((testCase) => {
    expect(averageColour(testCase.slice(0, 3))).toBe(testCase[3]);
  });
});
