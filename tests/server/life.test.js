const Life = require('../../server/life');


// these are from https://en.wikipedia.org/wiki/Conway's_Game_of_Life#Examples_of_patterns

const BLOCK = [
  '    ',
  ' XX ',
  ' XX ',
  '    ',
];

const BEEHIVE = [
  '      ',
  '  XX  ',
  ' X  X ',
  '  XX  ',
  '      ',
];

const LOAF = [
  '      ',
  '  XX  ',
  ' X  X ',
  '  X X ',
  '   X  ',
  '      ',
];

const STILL_LIFES = { BLOCK, BEEHIVE, LOAF };

const BLINKER = [
  [
    '     ',
    '     ',
    ' XXX ',
    '     ',
    '     ',
  ],
  [
    '     ',
    '  X  ',
    '  X  ',
    '  X  ',
    '     ',
  ],
];

const BEACON = [
  [
    '      ',
    ' XX   ',
    ' X    ',
    '    X ',
    '   XX ',
    '      ',
  ],
  [
    '      ',
    ' XX   ',
    ' XX   ',
    '   XX ',
    '   XX ',
    '      ',
  ],
];

const PULSAR = [
  [
    '                 ',
    '                 ',
    '    XXX   XXX    ',
    '                 ',
    '  X    X X    X  ',
    '  X    X X    X  ',
    '  X    X X    X  ',
    '    XXX   XXX    ',
    '                 ',
    '    XXX   XXX    ',
    '  X    X X    X  ',
    '  X    X X    X  ',
    '  X    X X    X  ',
    '                 ',
    '    XXX   XXX    ',
    '                 ',
    '                 ',
  ],
  [
    '                 ',
    '     X     X     ',
    '     X     X     ',
    '     XX   XX     ',
    '                 ',
    ' XXX  XX XX  XXX ',
    '   X X X X X X   ',
    '     XX   XX     ',
    '                 ',
    '     XX   XX     ',
    '   X X X X X X   ',
    ' XXX  XX XX  XXX ',
    '                 ',
    '     XX   XX     ',
    '     X     X     ',
    '     X     X     ',
    '                 ',
  ],
  [
    '                 ',
    '                 ',
    '    XX     XX    ',
    '     XX   XX     ',
    '  X  X X X X  X  ',
    '  XXX XX XX XXX  ',
    '   X X X X X X   ',
    '    XXX   XXX    ',
    '                 ',
    '    XXX   XXX    ',
    '   X X X X X X   ',
    '  XXX XX XX XXX  ',
    '  X  X X X X  X  ',
    '     XX   XX     ',
    '    XX     XX    ',
    '                 ',
    '                 ',
  ],
];

const OSCILLATORS = { BLINKER, BEACON, PULSAR };


function patternToCoords(pattern) {
  const [w, h] = [pattern[0].length, pattern.length];
  const coords = [];
  for (let y = 0; y < h; y += 1) {
    for (let x = 0; x < w; x += 1) {
      if (pattern[y][x] === 'X') coords.push([x, y]);
    }
  }
  return { w, h, coords };
}

function isBoardAndPatternEqual(board, pattern) {
  for (let y = 0; y < board.height; y += 1) {
    for (let x = 0; x < board.width; x += 1) {
      const boardEmpty = board.rows[y][x] === board.emptyValue;
      const patternEmpty = pattern[y][x] !== 'X';
      if (boardEmpty !== patternEmpty) return false;
    }
  }
  return true;
}


/* globals test, expect */
Object.keys(STILL_LIFES).forEach((key) => {
  test(`Still life: ${key}`, () => {
    const pattern = STILL_LIFES[key];
    const { w, h, coords } = patternToCoords(pattern);
    const game = new Life(w, h);
    game.queueBirths(0x0, coords);
    let stats = game.nextGeneration();
    expect(stats.births).toBe(coords.length);
    expect(stats.deaths).toBe(0);
    expect(game.board.population).toBe(coords.length);

    for (let i = 0; i < 10; i += 1) {
      stats = game.nextGeneration();
      expect(stats.births).toBe(0);
      expect(stats.deaths).toBe(0);
      expect(isBoardAndPatternEqual(game.board, pattern)).toBe(true);
    }

    expect(game.board.population).toBe(coords.length);
    expect(isBoardAndPatternEqual(game.board, pattern)).toBe(true);
  });
});


Object.keys(OSCILLATORS).forEach((key) => {
  test(`Oscillator: ${key}`, () => {
    const patterns = OSCILLATORS[key];
    const { w, h, coords } = patternToCoords(patterns[0]);
    const game = new Life(w, h);
    game.queueBirths(0x0, coords);

    for (let i = 0; i < 42; i += 1) {
      game.nextGeneration();
      const expectedPattern = patterns[i % patterns.length];
      expect(isBoardAndPatternEqual(game.board, expectedPattern)).toBe(true);
    }
  });
});
