const Board = require('../../server/board');

/* globals test, expect */
test('board size', () => {
  const testCases = [[16, 9]];
  testCases.forEach((testCase) => {
    const b = new Board(testCase[0], testCase[1]);
    expect(b.width).toBe(testCase[0]);
    expect(b.height).toBe(testCase[1]);
    expect(b.rows.length).toBe(b.height);
    b.rows.forEach((row) => {
      expect(row.length).toBe(b.width);
    });
  });
});

test('board clear', () => {
  const b = new Board(16, 16);
  b.clear();
  b.rows.forEach((row) => {
    row.forEach((cell) => {
      expect(cell).toBe(b.emptyValue);
    });
  });
});
