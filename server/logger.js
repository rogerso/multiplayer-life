const winston = require('winston');
const config = require('./config');

const logger = new (winston.Logger)({
  level: config.LOG_LEVEL,
  transports: [
    new winston.transports.Console({ colorize: true }),
  ],
});

module.exports = logger;
