const Board = require('./board');
const averageColour = require('./averageColour');


// Given board and (x, y) coordinates
// returns the list of colours of alive cells in the 8 neighbours.
//
// The neighbour detection wraps around the edges
// to give an "infinite" universe.
function getNeighbourColours(board, x, y) {
  const w = board.width;
  const h = board.height;
  const xRange = [(x - 1 + w) % w, x, (x + 1) % w];
  const yRange = [(y - 1 + h) % h, y, (y + 1) % h];
  const colours = [];
  yRange.forEach((j) => {
    const row = board.rows[j];
    const isCurrentRow = y === j;
    xRange.forEach((i) => {
      if (x !== i || !isCurrentRow) {
        const colour = row[i];
        if (colour !== board.emptyValue) colours.push(colour);
      }
    });
  });
  return colours;
}


// The Game of Life logic is implemented in this class
class Life {
  constructor(width, height) {
    this.board = new Board(width, height); // current universe
    this.nextBoard = new Board(width, height); // workspace to calculate next step
    this.generation = 0; // current generation #
    this.board.population = 0;
    this.birthQueue = [];
  }

  queueBirths(colour, coords) {
    this.birthQueue.push([colour, coords]);
  }

  // calculates the next game state into nextBoard, then swaps board and nextBoard
  // queued births are processed after the life of current cells have been evaluated
  // returns the number of births and deaths in this step
  // TODO: also return a list of coordinates of the births and deaths
  nextGeneration() {
    this.nextBoard.clear();
    const [w, h] = [this.board.width, this.board.height];
    let [pop, births, deaths] = [0, 0, 0];
    if (this.board.population > 0) {
      for (let j = 0; j < h; j += 1) {
        const currentRow = this.board.rows[j];
        const nextRow = this.nextBoard.rows[j];
        for (let i = 0; i < w; i += 1) {
          const neighbours = getNeighbourColours(this.board, i, j);
          const currentValue = currentRow[i];
          let nextValue = currentValue;
          switch (neighbours.length) {
            case 2:
              if (currentValue !== this.board.emptyValue) pop += 1;
              break;
            case 3:
              if (currentValue === this.board.emptyValue) {
                nextValue = averageColour(neighbours);
                births += 1;
              }
              pop += 1;
              break;
            default:
              if (currentValue !== this.board.emptyValue) {
                nextValue = this.board.emptyValue;
                deaths += 1;
              }
          }
          nextRow[i] = nextValue;
        }
      }
    }
    this.birthQueue.forEach(([colour, coords]) => {
      coords.forEach(([x, y]) => {
        // out of bounds?
        if (y >= w || x >= h) return;
        const row = this.nextBoard.rows[y];
        // only give birth if current cell is not occupied
        if (row[x] === this.board.emptyValue) {
          row[x] = colour;
          pop += 1;
          births += 1;
        }
      });
    });
    this.birthQueue = [];
    const prevBoard = this.board;
    this.board = this.nextBoard;
    this.nextBoard = prevBoard;
    this.board.population = pop;

    // if absolutely no activity, reset generation # to zero
    if (pop === 0 && births === 0 && deaths === 0) {
      this.generation = 0;
    } else {
      this.generation += 1;
    }

    return { births, deaths };
  }

  toJSON() {
    return {
      gen: this.generation,
      pop: this.board.population,
      board: this.board.toJSON(),
    };
  }
}

module.exports = Life;
