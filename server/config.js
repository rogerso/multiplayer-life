const Config = {
  PORT: Number(process.env.PORT) || 3000,
  TICK_INTERVAL: Number(process.env.TICK_INTERVAL) || 500,
  BOARD_WIDTH: Number(process.env.BOARD_WIDTH) || 256,
  BOARD_HEIGHT: Number(process.env.BOARD_HEIGHT) || 192,
  LOG_LEVEL: process.env.LOG_LEVEL || 'info',
  JWT_SECRET: process.env.JWT_SECRET || 'FIXME: put a real secret string here',
};

module.exports = Config;
