// Given an array of colours (in RGB format), returns the average
// as calculated per element

/* eslint-disable no-bitwise */
function averageColour(colours) {
  const [red, green, blue] = colours.reduce(([rTotal, gTotal, bTotal], colour) => {
    const r = rTotal + (((colour & 0x00ff0000) >> 16) & 0xff);
    const g = gTotal + (((colour & 0x0000ff00) >> 8) & 0xff);
    const b = bTotal + ((colour & 0x000000ff) & 0xff);
    return [r, g, b];
  }, [0, 0, 0]);
  const count = colours.length;
  return ((red / count) << 16) + ((green / count) << 8) + (blue / count);
}
/* eslint-enable no-bitwise */

module.exports = averageColour;
