const assert = require('assert');
const logger = require('./logger');

// empty value = white RGB
const EMPTY_VALUE = 0x00ffffff;

// A Board is where a Game of Life game runs on
//
// Currently implemented as a 2D array.
// Each cell is a 32-bit integer to hold 24-bit RGB colour value.
//
// As a small optimisation, the entire board is backed by one single buffer.
// Each row's array is a view into this buffer such that each cell can still
// be accessed easily using the rows[y][x] idiom.
class Board {
  constructor(width, height, emptyVal = EMPTY_VALUE) {
    assert(width > 0 && height > 0);
    this.width = width;
    this.height = height;
    this.emptyValue = emptyVal;
    const buffer = new ArrayBuffer(width * height * 4);
    this.rows = new Array(height);
    for (let i = 0; i < height; i += 1) {
      const row = new Uint32Array(buffer, i * width * 4, width);
      row.fill(emptyVal);
      this.rows[i] = row;
    }
    logger.log('verbose', 'board created, size %d x %d, emptyValue = %s', width, height, (`000000${emptyVal.toString(16)}`).slice(-6));
  }

  clear() {
    this.rows.forEach(row => row.fill(this.emptyValue));
  }

  // Each row is a map of non-empty cells, with x coordinate as key.
  // This is to avoid sending empty cells, which would save bandwidth based
  // on the assumption that most Life games have large empty areas.
  toJSON() {
    return {
      width: this.width,
      height: this.height,
      rows: this.rows.map(row => row.reduce((s, cell, i) => {
        if (cell !== this.emptyValue) {
          s[i] = (cell & 0xffffff).toString(16); // eslint-disable-line no-bitwise
        }
        return s;
      }, {})),
    };
  }
}

module.exports = Board;
