const path = require('path');
const express = require('express');
const hslToRgb = require('hsl-to-rgb-for-reals');
const jwt = require('jsonwebtoken');
const Life = require('./life');
const Config = require('./config');
const logger = require('./logger');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server).of('/life');


// game loop
const game = new Life(Config.BOARD_WIDTH, Config.BOARD_HEIGHT);
function iterateGame() {
  game.nextGeneration();
  logger.log('debug', 'generation %d calculated', game.generation);
  io.emit('t', game.toJSON());
}
setInterval(iterateGame, Config.TICK_INTERVAL);


// generate a random colour
// To generate a colour that is neither too bright nor too dark, we use the HSL colorspace to
// have a colour with 0.5-1.0 saturation and 0.2-0.9 lightness
function randomColour() {
  const [r, g, b] = hslToRgb(Math.floor(Math.random() * 360), Math.random() * 0.5 + 0.5, Math.random() * 0.7 + 0.2);
  return ((r & 0xff) << 16) + ((g & 0xff) << 8) + (b & 0xff); // eslint-disable-line no-bitwise
}


app.use(express.static(path.join(__dirname, '../client/public')));

io.on('connection', (socket) => {
  const colour = (() => {
    const { token } = socket.handshake.query;
    if (token) {
      try {
        const decoded = jwt.verify(token, Config.JWT_SECRET, { algorithms: ['HS384'] });
        if (decoded && decoded.colour) {
          const c = Number(decoded.colour);
          if (!Number.isNaN(c)) return c;
        }
      } catch (err) {
        logger.log('info', 'could not decode token, ignored');
      }
    }
    return randomColour();
  })();
  const colourHex = colour.toString(16);

  const log = (level, msg) => {
    logger.log(level, '[#%s] %s', (`000000${colourHex}`).slice(-6), msg);
  };

  log('info', 'connected');

  const token = jwt.sign({ colour }, Config.JWT_SECRET, { algorithm: 'HS384' });
  socket.emit('init', {
    token, version: 1, colour: colourHex, width: Config.BOARD_WIDTH, height: Config.BOARD_HEIGHT,
  });

  socket.on('disconnect', () => {
    log('info', 'disconnected');
  });

  socket.on('birth', (coords) => {
    log('info', `giving birth to ${coords.length} cells`);
    game.queueBirths(colour, coords);
  });
});

server.listen(Config.PORT, () => {
  logger.log('info', `listening on port ${Config.PORT}`);
});
